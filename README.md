# Welcome!

Thanks for joining - if you're seeing this you've decided to help build Nullible.

Discord chat can be found [here](https://discordapp.com/channels/572086036255997975/572086036255997981).

The pinned posts in the discord chat explain the purpose and intentions of this site, please refer to them.

----

## Getting Started

In the project directory, you can run:

### `npm start`

This will start the app on port 3000.
Open [http://localhost:3000](http://localhost:3000) to view it in the browser.


The page will reload if you make edits.
You will also see any lint errors in the console.