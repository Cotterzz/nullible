import firebase from 'firebase/app';
import 'firebase/firestore';
import 'firebase/auth';

var config = {
  apiKey: "AIzaSyAjQdUVhmTxhSpOFGCvUP08fvdyCHbX9PQ",
  authDomain: "nullible-5d4e5.firebaseapp.com",
  databaseURL: "https://nullible-5d4e5.firebaseio.com",
  projectId: "nullible-5d4e5",
  storageBucket: "nullible-5d4e5.appspot.com",
  messagingSenderId: "761912072721"
};
firebase.initializeApp(config);

export default firebase;