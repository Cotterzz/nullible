import React, { Component } from 'react'; 
import { connect } from 'react-redux';
import { Redirect } from 'react-router-dom';

import { signUp } from '../../store/actions/authActions';

class SignUp extends Component {

	state = {
		firstName: "",
		lastName: "",
		email: "",
		password: ""
	}

	handleChange = (e) => {
		this.setState({
			[e.target.id]: e.target.value
		});
	}

	handleSubmit = (e) => {
		e.preventDefault();
		this.props.signUp(this.state);
	}

	render() {
		const { auth, authError } = this.props;

		if (auth.uid) return <Redirect to="/" />

		return (
	  	<div className="container">
		  	<div className="row">
		  		<div className="col s12 m6 offset-m3">
		  		<form onSubmit={this.handleSubmit} className="white" autoComplete="off" value="">
		  			<h4 className="grey-text text-darken-3">Register</h4>
		  			<div className="input-field">
		  				<input type="text" id="fullName" onChange={this.handleChange}/>
		  				<label htmlFor="fullName">Full Name</label>
		  			</div>
		  			<div className="input-field">
		  				<input type="text" id="username" onChange={this.handleChange}/>
		  				<label htmlFor="username">Username</label>
		  			</div>
		  			<div className="input-field">
		  				<input type="email" id="email" onChange={this.handleChange}/>
		  				<label htmlFor="email">Email</label>
		  			</div>
		  			<div className="input-field">
		  				<input type="password" id="password" onChange={this.handleChange}/>
		  				<label htmlFor="password">Password</label>
		  			</div>
		  			<div className="input-field">
		  				<button className="btn green lighten-1 z-depth-1 right">Register</button>
		  				{authError && <p className="red-text">{authError}</p>}
		  			</div>
	  			</form>
		  		</div>
		  	</div>
	  	</div>
 		)
	}
}

const mapStateToProps = (state) => {
	console.log(state);
	return {
		auth: state.firebase.auth,
		authError: state.auth.authError
	}
}

const mapDispatchToProps = (dispatch) => {
	return {
		signUp: (newUser) => dispatch(signUp(newUser))
	}
}

export default connect(mapStateToProps, mapDispatchToProps)(SignUp);