import React, { Component } from 'react'; 
import { connect } from 'react-redux';
import { Redirect } from 'react-router-dom';

import { signIn } from '../../store/actions/authActions';

class SignIn extends Component {

	state = {
		email: "",
		password: ""
	}

	handleChange = (e) => {
		this.setState({
			[e.target.id]: e.target.value
		});
	}

	handleSubmit = (e) => {
		e.preventDefault();
		this.props.signIn(this.state);
	}

	render() {
		const { authError, auth } = this.props;

		if (auth.uid) return <Redirect to="/" />

		return (
	  	<div className="login container">
	  		<div className="row">
		  		<div className="col s12 m6 offset-m3">
			  		<form onSubmit={this.handleSubmit} className="white">
			  			<h4 className="grey-text text-darken-3">Login</h4>
			  			<div className="input-field">
			  				<input type="email" id="email" onChange={this.handleChange} required/>
			  				<label htmlFor="email">Email</label>
			  			</div>
			  			<div className="input-field">
			  				<input type="password" id="password" onChange={this.handleChange} required/>
			  				<label htmlFor="password">Password</label>
			  			</div>
			  			<div className="input-field">
			  				<button className="btn blue lighten-1 z-depth-1 right">Login</button>
			  				<div className="red-text center">
			  					{ authError && <p>{authError}</p>}
			  				</div>
			  			</div>
			  		</form>
		  		</div>
		  	</div>
	  	</div>
 		)
	}
}

const mapStateToProps = (state) => {
	return {
		auth: state.firebase.auth,
		authError: state.auth.authError
	}
}

const mapDispatchToProps = (dispatch) => {
	return {
		signIn: (creds) => dispatch(signIn(creds))
	}
}

export default connect(mapStateToProps, mapDispatchToProps)(SignIn);