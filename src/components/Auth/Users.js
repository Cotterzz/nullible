import React from 'react';
import { connect } from 'react-redux';

import { getUser } from '../../store/actions/authActions';

class Users extends React.Component {

	constructor(props) {
		super(props);
		this.state = {
			userId: props.match.params.id
		}
	}

	componentDidMount() {
		const userId = this.state.userId;
		if (!userId) { return };
		this.props.getUser(userId);
	}

	render() {
		const { user, error } = this.props;
		return (
			<div className="container section">
				<div className="card z-depth-0">
					<div className="card-content">
					<span className="card-title">
						{user && user.fullName}
						{error && error}
					</span>
					<h6>Latest Articles</h6>
					<p>Work in progress...</p>
					</div>
				</div>
			</div>
		);
	}
//{ profile && <p>{profile.firstName} {profile.lastName}</p> }
}

const mapStateToProps = (state) => {
	return {
		user: state.auth.userData,
		error: state.auth.userError
	}
}

const mapDispatchToProps = (dispatch) => {
	return {
		getUser: (userId) => dispatch(getUser(userId))
	}
}

export default connect(mapStateToProps, mapDispatchToProps)(Users);