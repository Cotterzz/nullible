import React from 'react';
import { Redirect } from 'react-router-dom';
import { connect } from 'react-redux';

// import ProjectList from '../projects/ProjectList';

const Profile = (props) => {

	const { profile } = props;

	if (!props.profile) return <Redirect to='/' />
	return (
		<div className="container section">
			<div className="card z-depth-1">
				<div className="card-content">
				<span className="card-title text-light-blue">
					{ profile && <p>{profile.fullName}</p> }
				</span>
				<h6>Latest Articles</h6>
				</div>
			</div>
		</div>
	);
}

const mapStateToProps = (state) => {
	return {
		auth: state.firebase.auth,
		profile: state.firebase.profile,
		projects: state.firestore.ordered.projects,
	}
}

export default connect(mapStateToProps)(Profile);
