import React from 'react';
import { connect } from 'react-redux';
import { compose } from 'redux';
import { firestoreConnect } from 'react-redux-firebase';

import ArticleList from '../Articles/ArticleList';

const Homepage = (props) => {
	const {articles} = props;
	return (
		<div className="home container">
			<div className="row">
				<div className="col s12 m8 l6">
					{/* Articles list */}
					<ArticleList articles={articles} />
				</div>
			</div>
		</div>
	);
};

const mapStateToProps = (state) => {
	return {
		articles: state.firestore.ordered.articles
	}
}

export default compose(
	connect(mapStateToProps),
	firestoreConnect([
		{ collection: 'articles', orderBy: ['createdAt', 'desc'] }
	])
)(Homepage)