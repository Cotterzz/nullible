import React, { Component } from 'react';
import { connect } from 'react-redux';
import { compose } from 'redux';
import { firestoreConnect } from 'react-redux-firebase';
import { getArticleVersions } from '../../store/actions/articleActions';

import ArticleVersionList from './ArticleVersionList';

class ArticleHistory extends Component {

	componentDidMount() {
		const { articleId } = this.props;
		if (!articleId) return;
		this.props.getArticleVersions(articleId);
	}

	render() {
		const { versions } = this.props;
		if (!versions) return <h3>Loading article version history...</h3>;
		if (versions.length === 0) return <h5 className="center">No version history for this article.</h5>;

		return (
			<div className="container article-history">
				<h1>Article History</h1>
				<hr />
				<div className="row">
					<div className="col s12 m8 l6">
						<ArticleVersionList versions={versions} />
					</div>
				</div>
			</div>
		);
	};
};

const mapStateToProps = (state, ownProps) => {
	const articleId = ownProps.match.params.id;
	return {
		articleId,
		versions: state.article.versions
	}
}

const mapDispatchToProps = (dispatch) => {
	return {
		getArticleVersions: (articleId) => dispatch(getArticleVersions(articleId))
	}
}

export default compose(
	connect(mapStateToProps, mapDispatchToProps),
	firestoreConnect([
		{ collection: 'article_histories', orderBy: ['createdAt', 'desc'] }
	])
)(ArticleHistory)