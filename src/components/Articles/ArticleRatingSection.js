import React, { Component } from 'react';
import { compose } from 'redux';
import { connect } from 'react-redux';
import { updateArticleScore } from '../../store/actions/articleActions';


class ArticleRatingSection extends Component {

	constructor(props) {
		super(props);

		this.state = {
			userId: props.userId,
			score: props.score - props.vote || 0,
			vote: props.vote || 0
		}
	}

	handleUpvote = () => {
		this.state.vote === 1 ? this.setVoteTo(0) : this.setVoteTo(1);
	}

	handleDownvote = () => {
		this.state.vote === -1 ? this.setVoteTo(0) : this.setVoteTo(-1);
	}

	setVoteTo(vote) {
    this.setState({vote}, () => this.updateArticleScoreWith(vote));
  }

  updateArticleScoreWith(vote) {
  	this.props.updateArticleScore(this.props.articleId, this.props.userId, vote);
  }

	render() {
		const { score, vote } = this.state;

		return (
			<div className="article-rating-section">
	    	<button className="material-icons" style={{color: vote > 0 ? '' : '#aaa'}} onClick={this.handleUpvote}>expand_less</button>
		    <span className="count">{score + vote}</span>
	    	<button className="material-icons" style={{color: vote < 0 ? '' : '#aaa'}} onClick={this.handleDownvote}>expand_more</button>
	    </div>
		);
	}
}

const mapDispatchToProps = (dispatch) => {
	return {
		updateArticleScore: (articleId, userId, vote) => dispatch(updateArticleScore(articleId, userId, vote))
	}
}

export default compose(connect(null, mapDispatchToProps))(ArticleRatingSection);