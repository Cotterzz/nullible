import React, { Component } from 'react';
import { Redirect } from 'react-router-dom';
import { connect } from 'react-redux';
import RichTextEditor from 'react-rte';


// import { database, auth } from '../Config';
// import addTagInputs, { getTags, addTagsToInput } from '../js/tags.js';

class EditArticle extends Component {

  state = {
    value: this.props.value,
    didChange: false
  }

  showLogin = () => {
    this.props.history.push('/login', {errorMsg: "You must be logged in to edit articles."});
  }

  onChange = (value) => {
    this.setState({value});
  };

  handleSubmit = (e) => {
    e.preventDefault();
    // this.props.updateArticle({articleId, content: this.state.value.toString('html')}); // TODO: Purify!
    this.props.history.goBack();
  }

  render() {
    const { auth, error, article = {} } = this.props;

    if (!auth.uid) return <Redirect to="/login" />
    if (!article) return <Redirect to="/" />

    return (
      <div className="container">
        <button className="btn-small" onClick={this.props.history.goBack}><i className="material-icons left">chevron_left</i>Go Back</button>
        <form onSubmit={this.handleSubmit}>
          <h3 className="grey-text text-darken-3">Edit Article</h3>
          <input placeholder={article.title} defaultValue={article.title} type="text" id="title" onChange={this.handleChange} required/>
          <RichTextEditor
            className="rich-text-editor"
            id="content"
            value={this.state.value}
            onChange={this.onChange}
          />
          <div className="input-field">
            <button className="btn pink waves-effect lighten-1 right">Submit</button>
            {error && <p>{error}</p>}
          </div>
        </form>
      </div>
    )
  }
}

const mapStateToProps = (state, ownProps) => {
  if (!state || !ownProps) return {};
  const articleId = ownProps.match.params.id;
  const { article } = ownProps.location.state;
  return {
    auth: state.firebase.auth,
    articleId,
    article,
    didChange: false,
    error: undefined,
    value: RichTextEditor.createValueFromString(article.content, 'html')
  }
}

export default connect(mapStateToProps)(EditArticle);


// updateArticle(event) {
  //   event.preventDefault();
  //   const id = this.props.match.params.id;
  //   const title = this.titleInput.value;
  //   const text = this.state.value.toString('html');
  //   const tags = getTags();
  //   // const {contributors} = this.props.location.state.article;
  //   // contributors.push(this.user.name); --  need to avoid duplicates

  //   database.ref('articles/' + id).update({
  //     title: title,
  //     text: text,
  //     tags: tags,
  //     // creator: creatorName
  //   }).then(() => {
  //     this.setState({ message: "Successfully updated article!" });
  //   }, (error) => {
  //     this.setState({ error: error.message });
  //   });
  //   // TODO: Need to update tags.
  //   // Give x/1000 seconds to search for user.
  //   setTimeout(function() {
  //     this.props.history.goBack()
  //   }.bind(this),2000);
  // }