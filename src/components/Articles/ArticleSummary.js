import React, { Component } from 'react';
import { Link } from 'react-router-dom';
import moment from 'moment';

export default class ArticleSummary extends Component {
	render() {
		const { article } = this.props;
		return (
			<div className="card z-depth-0 article-summary">
				<div className="card-content grey-text text-darken-3">
					<h5><Link className="teal-text text-accent-4" to={article.id ? `/article/${article.id}`: '/notfound'}>{article.title}</Link></h5>
					{article.authorFullName && <p>Posted by <Link className="user-link" to={`/users/${article.authorId}`}>{article.authorFullName}</Link></p>}
					{article.createdAt && <p className="grey-text">{moment(article.createdAt.toDate()).calendar()}</p>}
				</div>
			</div>
		);
	}
}
