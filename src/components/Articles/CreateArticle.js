import React, { Component } from 'react';
import { connect } from 'react-redux';
import { createArticle } from '../../store/actions/articleActions';
import { Redirect } from 'react-router-dom';
import RichTextEditor from 'react-rte';

class CreateArticle extends Component {

	state = {
		title: "",
		content: "",
		value: RichTextEditor.createEmptyValue()
	}

	handleChange = (e) => {
		this.setState({
			[e.target.id]: e.target.value
		});
	}

	handleSubmit = (e) => {
		e.preventDefault();
		this.props.createArticle({title:this.state.title, content: this.state.value.toString('html')});
		this.props.history.push('/');
	}

	onChange = (value) => {
    this.setState({value});
  };

	render() {
		const { auth } = this.props;
		const error = auth.error;
		if (!auth.uid) return <Redirect to="/login" />
		return (
	  	<div className="container">
	  		<form onSubmit={this.handleSubmit}>
	  			<h3 className="grey-text text-darken-3">New Article</h3>
	  			<div className="input-field">
	  				<label htmlFor="title">Title</label>
	  				<input type="text" id="title" onChange={this.handleChange} required/>
	  			</div>
	  			<RichTextEditor
						className="rich-text-editor"
						id="content"
		        value={this.state.value}
		        onChange={this.onChange}
		      />
	  			<div className="input-field">
	  				<button className="btn pink waves-effect lighten-1 z-depth-1 right">Create</button>
	  				{error && <p>{error}</p>}
	  			</div>
	  		</form>
	  	</div>
 		)
	}
}
  				// <div className="input-field">
	  			// 	<label htmlFor="content">Content</label>
	  			// 	<textarea id="content" className="materialize-textarea" onChange={this.handleChange} required/>
	  			// </div>

const mapStateToProps = (state) => {
	return {
		auth: state.firebase.auth
	}
}

const mapDispatchToProps = (dispatch) => {
	return {
		createArticle: (article) => dispatch(createArticle(article))
	}
}

export default connect(mapStateToProps, mapDispatchToProps)(CreateArticle);