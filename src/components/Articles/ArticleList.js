import React from 'react';
import { Link } from 'react-router-dom';

import ArticleSummary from './ArticleSummary';

const ArticleList = (props) => {
	const { articles } = props;
	if (!articles) return <h3>Loading articles...</h3>;
	return (
		<div className="article-list section">
			{articles && articles.length > 0 
				? 
					articles.map(article => {
						return <ArticleSummary article={article} key={article.id} />
					})
				: 
					<span>No articles :-( <Link to='/create'>Click here to make one!</Link></span>
			}
		</div>
	);
}

export default ArticleList;