import React from 'react';
import { Link } from 'react-router-dom';

import ArticleSummary from './ArticleSummary';

const ArticleVersionList = (props) => {

	const { versions } = props;
	if (!versions) return <h3>Loading...</h3>;
	return (
		<div className="article-list section">
			{ versions && versions.length > 0
				?
					versions.map(article => {
						return <ArticleSummary article={article} key={article.title} />
					})
				: 
					<span>No articles :-( <Link to='/create'>Click here to make one!</Link></span>
			}
		</div>
	);
}

export default ArticleVersionList;