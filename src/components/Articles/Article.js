import React, { Component } from 'react';
import { connect } from 'react-redux';
import { firestoreConnect } from 'react-redux-firebase';
import { compose } from 'redux';
import { Link, Redirect } from 'react-router-dom';
import moment from 'moment';
import createDOMPurify from 'dompurify';

import ArticleRatingSection from './ArticleRatingSection';

import './Article.css';

const DOMPurify = createDOMPurify(window);

class Article extends Component {

	render() {
		const { auth, articleId, article, userVotes } = this.props;
		const userId = auth.uid;
		if (!userId) return <Redirect to="/login" />

		if (!article) return <div className="container center"><p>Loading article...</p></div>;
		if (!userVotes) return <div className="container center"><p>Loading votes...</p></div>;
		const vote = userVotes[userId][articleId] || 0;

		return (
			<div className="container article">
				<div className="article-header">
					<div className="article-header-flex">
						<div className="article-header-left">
							<h3 className="article-title">{article.title}</h3>
							<div className="meta-info">
								<p>Posted by <Link className="user-link" to={`/users/${article.authorId}`}>{article.authorFullName}</Link></p>
								<div>{moment(article.createdAt.toDate()).calendar()}</div>
							</div>
						</div>
						<ArticleRatingSection articleId={this.props.articleId} userId={userId} score={article.score} vote={vote} />
					</div>
					<Link className="teal-text text-accent-4" to={{pathname:`/edit/article/${articleId}`, state:{ article, articleId }}}>Edit this article</Link>
					<Link className="teal-text text-accent-4 right" to={{pathname:`/history/article/${articleId}`, state:{ articleId }}}>View article history</Link>
				</div>
				<hr />
				<div className="article-content" dangerouslySetInnerHTML={{ __html: DOMPurify.sanitize(article.content) }} />
			</div>
		);
	}
}

const mapStateToProps = (state, ownProps) => {
	const articles = state.firestore.data.articles;
	const articleId = ownProps.match.params.id;
	const article = articles ? articles[articleId] : null;
	return {
		auth: state.firebase.auth,
		articleId,
		article,
		userVotes: state.firestore.data.user_votes,
	}
}

export default compose(
	connect(mapStateToProps),
	firestoreConnect([
		{collection: 'articles'},
		{collection: 'user_votes'}
	])
)(Article);