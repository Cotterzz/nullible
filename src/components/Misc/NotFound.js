import React from "react";

export default () =>
  <div style={{textAlign:'center'}}>
    <h3>Sorry, page not found!</h3>
    <a href="/">Click here to go home</a>
  </div>;
