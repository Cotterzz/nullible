import React from 'react';
import { NavLink } from 'react-router-dom';

const SignedOutLinks = () => {
	return (
		<ul id="nav-mobile" className="right hide-on-med-and-down">
			<li><NavLink className="grey-text text-darken-4" to='/register'>Register</NavLink></li>
			<li><NavLink className="grey-text text-darken-4" to='/login'>Login</NavLink></li>
		</ul>
	)
}

export default SignedOutLinks;