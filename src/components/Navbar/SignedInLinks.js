import React from 'react';
import { NavLink } from 'react-router-dom';
import { connect } from 'react-redux';
import { signOut } from '../../store/actions/authActions';

const SignedInLinks = (props) => {
	return (
		<ul id="nav-mobile" className="right hide-on-med-and-down">
			<li><NavLink to='/profile' className="grey-text text-darken-4">Profile</NavLink></li>
			<li><NavLink to='/create' className="grey-text text-darken-4">Create</NavLink></li>
			<li><NavLink to="/" className="grey-text text-darken-4" onClick={props.signOut}>Logout</NavLink></li>
		</ul>
	)
}

const mapDispatchToProps = (dispatch) => {
	return {
		signOut: () => dispatch(signOut())
	}
}

export default connect(null, mapDispatchToProps)(SignedInLinks);