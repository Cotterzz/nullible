import React from 'react';
import {Link} from 'react-router-dom';
import { connect } from 'react-redux';

import SignedInLinks from './SignedInLinks';
import SignedOutLinks from './SignedOutLinks';

const Navbar = (props) => {
	const {auth, profile} = props;
	return (
		<div className="navbar">
			<nav className="nav-wrapper white grey-text">
				<div className="container grey-text">
					<Link to="/" className="brand-logo teal-text text-accent-4">Nullible</Link>
					{ auth.uid ? <SignedInLinks profile={profile} /> : <SignedOutLinks /> }
				</div>
			</nav>
		</div>
	);
}
// <a href="#" data-target="mobile-demo" className="btn-flat sidenav-trigger right"><i className="material-icons">menu</i></a>

const mapStateToProps = (state) => {
	return {
		auth: state.firebase.auth,
		profile: state.firebase.profile
	}
}

export default connect(mapStateToProps)(Navbar);