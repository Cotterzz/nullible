import React, { Component } from 'react';
import { BrowserRouter, Switch, Route } from 'react-router-dom';
import './App.css';

import Navbar from './components/Navbar/Navbar';
import Homepage from './components/Homepage/Homepage';
import Create from './components/Articles/CreateArticle';
import Article from './components/Articles/Article';
import EditArticle from './components/Articles/EditArticle';
import ArticleHistory from './components/Articles/ArticleHistory';
import ArticleVersion from './components/Articles/ArticleVersion';
import Users from './components/Auth/Users';
import Profile from './components/Auth/Profile';
import Login from './components/Auth/SignIn';
import Register from './components/Auth/SignUp';
import NotFound from './components/Misc/NotFound';

export default class App extends Component {
  render() {
    return (
      <BrowserRouter>
        <div className="app">
          <Navbar />
          <div className="main">
            <Switch>
              <Route exact path="/" component={Homepage} />

              <Route path="/create" component={Create} />

              <Route path="/article/:id" component={Article} />
              <Route path="/history/article/:id" component={ArticleHistory} />
              <Route path="/article/version/:id" component={ArticleVersion} />
              <Route path="/edit/article/:id" component={EditArticle} />

              <Route path="/users/:id" component={Users} />
              <Route path="/profile" component={Profile} />
              <Route path="/login" component={Login} />
              <Route path="/register" component={Register} />

              {/* Handle 404 errors. */}
              <Route component={NotFound} />
            </Switch>
          </div>
          <div className="footer">
            
          </div>
        </div>
      </BrowserRouter>
    );
  }
}