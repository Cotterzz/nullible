const articleReducer = (state = {}, action) => {
	switch(action.type) {
		case "CREATE_ARTICLE":
			return {...state, error: "something"};
		case "CREATE_ARTICLE_ERROR": 
			return {...state, error: action.error};
		case "UPVOTE_ARTICLE_SUCCESS":
			return {...state, error: null};
		case "UPVOTE_ARTICLE_ERROR":
			console.log("UPVOTE_ARTICLE_ERROR: ", action.error);
			return {...state, error: "Unable to vote on this article at this time."};
		case "ARTICLE_VERSIONS_FETCHED":
			return {...state, versions: action.versions, error: null};
		case "ARTICLE_VERSIONS_FETCH_ERROR":
			console.log("ARTICLE_VERSIONS_FETCH_ERROR", action.error);
			return {...state, error: action.error};
		default:
			return state;
	}
}

export default articleReducer;