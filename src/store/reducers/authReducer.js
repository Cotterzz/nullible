const authReducer = (state = {}, action) => {
	switch(action.type) {
		case "LOGIN_ERROR": return {...state, authError: "Login failed."};
		case "LOGIN_SUCCESS":  
			return {...state, authError: null}
		case "SIGNOUT_SUCCESS":
			return state;
		case "SIGNUP_SUCCESS":
			return {...state, authError: null};
		case "SIGNUP_ERROR":
			return {...state, authError: action.error.message};
		case "USER_FETCH_SUCCESS":
			return {...state, userData: action.user, userError: null};
		case "USER_FETCH_ERROR":
			return {...state, user: null, error: action.error};
		default: return state;
	}
}

export default authReducer;