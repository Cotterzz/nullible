export const createArticle = (article) => {
	return (dispatch, getState, { getFirebase, getFirestore }) => {
		const firestore = getFirestore();
		const profile = getState().firebase.profile;
		const authorId = getState().firebase.auth.uid
		firestore.collection('articles').add({
			...article,
			authorFullName: profile.fullName,
			authorId: authorId,
			score: 0,
			createdAt: new Date()
		}).then(() => {
			dispatch({type: 'CREATE_ARTICLE', article});
		}).catch((error) => {
			dispatch({type: 'CREATE_ARTICLE_ERROR', error});
		});
	}
};

export const updateArticleScore = (articleId, userId, vote) => {
	return (dispatch, getState, { getFirestore }) => {
		const firestore = getFirestore();
		const articleDocRef = firestore.collection('articles').doc(articleId);
		const userVotesDocRef = firestore.collection('user_votes').doc(userId);

		firestore.runTransaction((transaction) => { 
			return Promise.all([transaction.get(articleDocRef), transaction.get(userVotesDocRef)]).then((docs) => {
				let articleRef = docs[0];
				let userVotesRef = docs[1];
				if (!articleRef.exists) { throw "Article does not exist." };

				const oldScore = articleRef.data().score;
				const oldVote = userVotesRef.data()[articleId] || 0;

				const newScore = oldScore - oldVote + vote;
	      transaction.update(articleDocRef, { score: newScore });

	      const userVote = {};
	      userVote[articleId] = vote;
	      transaction.update(userVotesDocRef, userVote);
			});
		})
		.then(() => {
			dispatch({type:'UPVOTE_ARTICLE_SUCCESS'})
		}).catch((error) => {
			dispatch({type:'UPVOTE_ARTICLE_ERROR', error})
		});
	}
};

export const getArticleVersions = (articleId) => {
	return (dispatch, getState, { getFirestore }) => {
		const firestore = getFirestore();
		const articleDocRef = firestore.collection('articles').doc(articleId);
		articleDocRef.collection('versions').get()
		.then((snap) => {
			// let versions = {}
			let versions = []
			snap.forEach((doc) => {	
				// versions[doc.id] = doc.data();
				versions.push(doc.data());
			});
			dispatch({type:"ARTICLE_VERSIONS_FETCHED", versions});
		})
		.catch((error) => {
			dispatch({type:"ARTICLE_VERSIONS_FETCH_ERROR", error});
		});
	}
};