export const signIn = (credentials) => {
	return (dispatch, getState, { getFirebase }) => {
		const firebase = getFirebase();

		firebase.auth().signInWithEmailAndPassword(
			credentials.email,
			credentials.password
		).then(() => {
			dispatch({type: "LOGIN_SUCCESS"});
		}).catch((error) => {
			dispatch({type: "LOGIN_ERROR", error});
		});
	}
}

export const signUp = (newUser) => {
	return (dispatch, getState, { getFirebase, getFirestore }) => {
		const firebase = getFirebase();
		const firestore = getFirestore();

		firebase.auth().createUserWithEmailAndPassword(
			newUser.email,
			newUser.password
		).then((response) => {
			return firestore.collection("users").doc(response.user.uid).set({
				fullName: newUser.fullName,
				username: newUser.username.toLowerCase(),
				initial: `${newUser.username[0]}`,
				score: 0
			})
		}).then(() => {
			dispatch({type:"SIGNUP_SUCCESS"});
		}).catch(error => {
			dispatch({type:"SIGNUP_ERROR", error});
		});
	}
}

export const signOut = () => {
	return (dispatch, getState, { getFirebase }) => {
		const firebase = getFirebase();
		firebase.auth().signOut().then(() => {
			dispatch({type: "SIGNOUT_SUCCESS"});
		});
	}
}

export const getUser = (userId) => {
	return (dispatch, getState, { getFirestore }) => {
		const firestore = getFirestore();
		firestore.collection("users").doc(userId).get().then((snap) => {
			const user = snap.data();
			dispatch({type:"USER_FETCH_SUCCESS", user});
		}).catch((error) => {
			dispatch({type:"USER_FETCH_ERROR", error});
		});
	}
}